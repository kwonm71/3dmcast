import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;
import java.util.zip.Inflater;


public class MMDEAClient extends Thread {

	ArrayList<Integer> viewsArr;
	int threadID;
	HashMap<Integer,String> responseViews;
    static JFrame frame=new JFrame();
	static JLabel lbl=new JLabel();
	static int viewsCount = 1;
	static Object synchronizationObject = new Object();

	public MMDEAClient(int threadID) {
		//displayCount =0;
		viewsArr = new ArrayList<Integer>();
		this.threadID = threadID;
		responseViews = new HashMap<Integer,String>();
	}

	public void generateRandomViews() {
        viewsArr.clear();
        int i = 0;
        while(i< 500){
        	int random = (int)((Math.random() * 100000) + 1);
            if(viewsArr.contains(random) == false)
            {
        	   viewsArr.add(i,random); 
               i++;
            }
        }
    }

    public void displayImages() {

    	synchronized(synchronizationObject) {

    	Set set = responseViews.entrySet();
        Iterator iterator = set.iterator();

        try {

        	while(iterator.hasNext()) {
        		Map.Entry me = (Map.Entry)iterator.next();   		

            	byte[] imgBytes = ((String) me.getValue()).getBytes("ISO-8859-1");

            	InputStream in = new ByteArrayInputStream(imgBytes);
        		BufferedImage bImageFromConvert = ImageIO.read(in);

            	ImageIcon icon=new ImageIcon(bImageFromConvert);
                
        		frame.setSize(500,500);
        		frame.setLayout(new FlowLayout());
        
        		lbl.setIcon(icon);
        		frame.add(lbl);
        		frame.setVisible(true);
        		frame.revalidate();
        		SwingUtilities.updateComponentTreeUI(frame);
        		sleep(100);
        		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);sleep(10);
        	}
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	}
    }

    public void startSending() {

    	while(true) {
    	generateRandomViews();

		Iterator it = viewsArr.iterator();

		System.out.println("sending");

		Collections.sort(viewsArr);

		try{

			InetAddress serverIp = InetAddress.getByName("129.21.37.28");
			int serverPort = 43555;

			Socket socket = new Socket(serverIp,serverPort);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(viewsArr);

			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			MMDEAClient displayObject = new MMDEAClient(1);
			displayObject.responseViews = (HashMap<Integer, String>) ois.readObject();
			//displayObject.start();
            System.out.println(displayObject.responseViews.size());


			System.out.println("received");

		} catch(Exception e) {
			e.printStackTrace();
		}}
	}

    public void run() {
    	if(threadID == 0) {
    		startSending();
    	}
    	else if(threadID == 1) {
    		displayImages();
    	}
    }

	public static void main(String[] args) {

		for(int i=0;i<5;i++) {

			try {
				MMDEAClient sendViews = new MMDEAClient(0);
				sendViews.start();
			}catch(Exception e) {
				System.out.println(e);
			}
		}

	}
}
