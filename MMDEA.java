/**
 * MMDEA.java
 *
 * This program implements the MMDEA dynamic algorithm
 * to calculate the minimum cost to send a set of
 * views to clients subscribed to them.
 */

import java.lang.*;
import java.io.*;
import java.util.*;
import java.net.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import java.util.zip.Deflater;


public class MMDEA extends Thread implements java.io.Serializable{
    
    //Array with set of views that the clients have subscribed
    static int noOfViews = 7;
    static int qualityConstant = 4;
    static HashMap<Integer,String> preProcessedImages = new HashMap<Integer,String>();

    //Hashmap to store start and end index of each segment
    HashMap<Integer, Integer> segmentPositions;
    TreeSet<Integer> viewsArr;
    TreeSet<Integer> viewsToSend;
    HashMap<Integer, String> imagesToSend;
    int requestSize;
    int noOfSegments;
    InetAddress clientAddress;
    int clientPort;
    Socket currentSocket;

    public MMDEA() {
        segmentPositions = new HashMap<Integer, Integer>();
        viewsArr = new TreeSet<Integer>();
        viewsToSend = new TreeSet<Integer>();
        imagesToSend = new HashMap<Integer,String>();
        requestSize = 0;
        noOfSegments = 1;
        clientPort = 0;
        currentSocket = null;
    }

    public static void startPreProcessing() {

        for(int j=1; j<=1000; j++) {System.out.println(j);

            try {
                int imageId = j;
                String name = "" + imageId;
                String temp = "";

                for(int i=0;i<(4-name.length());i++) {
                    temp += "0";
                }
                name = temp + name + ".png";
                name = "movieViews" + "/" + name;
                BufferedImage img=ImageIO.read(new File(name));

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(img, "png", baos);
                baos.flush();
                byte[] imageInByte = baos.toByteArray();
                baos.close();

                preProcessedImages.put(imageId, new String(imageInByte,"ISO-8859-1"));
            } catch(Exception e) {
                System.out.println(e);
            }
        } 

    }

    public void populateViewsToSend(int startIndex, int endIndex, int[] minCostArray) {

        int noViews = endIndex - startIndex + 1;

        viewsToSend.add(endIndex);
        viewsToSend.add(startIndex);

        int temp = minCostArray[1];

        for(int i=2;i<=noViews;i++) {

            if(minCostArray[i] != temp && minCostArray[i] < 9999) {
                temp = minCostArray[i];
                while(i<=noViews && temp == minCostArray[i]) {
                    i++;
                }

                if(i<=noViews) {
                    viewsToSend.add(startIndex+(i-2));
                    temp = minCostArray[i-1];
                }
            }
        }
    }
    
    public void createSegments() {

        int i=0;
        noOfSegments=1;
        Iterator it = viewsArr.iterator();

        int[] viewsArray = new int[requestSize];

        while(it.hasNext()) {

            viewsArray[i] = (Integer) it.next();
            i++;
        }

        i=0;
        int firstView = viewsArray[0];

        for(i=0;i<(viewsArray.length-1);i++) {
            if((viewsArray[i+1] - viewsArray[i]) > qualityConstant) {
                segmentPositions.put(firstView, viewsArray[i]);
                firstView = viewsArray[i+1];
                noOfSegments++;
            }            
        }

        segmentPositions.put(firstView, viewsArray[i]);
    }
    
    //Check if any client has subscribed to the current view
    public boolean isSubscribed(int view) {       
        
        Iterator it = viewsArr.iterator();
        int[] viewsArray = new int[requestSize];
        int i = 0;

        while(it.hasNext()) {

            viewsArray[i] = (Integer) it.next();
            i++;
        }

        for(i=0;i<viewsArray.length;i++) {            
            if(view == viewsArray[i]) {
                return true;
            }
        }        
        return false;
    }

    public void startComputations() {

        //The cost of sending a view is set as 7
        int sendingCost = 7;
        int[] minCostArray;
        int[][] dynamicCostArray;
        createSegments();

        int totalCost = 0;

        Set set = segmentPositions.entrySet();
        Iterator iterator = set.iterator();

        double startTime = System.currentTimeMillis();

        for(int v=0; v<noOfSegments; v++) {
                
            Map.Entry me = (Map.Entry)iterator.next();
            int startIndex = (Integer) me.getKey();
            int endIndex = (Integer) me.getValue();
                
            int noOfViews = endIndex - startIndex + 1;
                
            //Array to store min. cost of sending each view
            minCostArray = new int[noOfViews+1];
            dynamicCostArray = new int[noOfViews+1][qualityConstant+1]; 
            
            //Initialize arrays
            for(int i=0; i<=noOfViews;i++) {
                minCostArray[i] = 0;
                for(int j=0;j<=qualityConstant;j++) {
                    dynamicCostArray[i][j] = 0;
                }
            }

            //create dynamic array
            for(int k=1; k<=noOfViews; k++) {
                
                if(isSubscribed(startIndex + (k-1))) {
                    if((k-1) > 0) {
                        dynamicCostArray[k][0] = sendingCost + minCostArray[k-1];
                    }
                    else {
                        dynamicCostArray[k][0] = sendingCost;
                    }
                }
                else {
                    //No one has subscribed to this view.
                    dynamicCostArray[k][0] = 99999;
                    minCostArray[k] = dynamicCostArray[k][0];
                    continue;
                }
                
                minCostArray[k] = dynamicCostArray[k][0];
                
                for(int d=2; d<=Math.min(k-1,qualityConstant);d++) {
                    dynamicCostArray[k][d] = minCostArray[k-d] + sendingCost + 0 ;
                    
                    //store the minimum cost to send the current view
                    if(dynamicCostArray[k][d] < minCostArray[k]) {
                        minCostArray[k] = dynamicCostArray[k][d];
                    }
                }       
            }
            for(int i=0; i<=noOfViews;i++) {
                /*    for(int j=0;j<=qualityConstant;j++) {
                        System.out.print(dynamicCostArray[i][j] + "  ");
                    }
                    System.out.println("");
                }*/
                System.out.print(minCostArray[i] + " ");
            }
            System.out.println("\n\n\n");

            populateViewsToSend(startIndex, endIndex, minCostArray);

            //store cost of sending each segment
            totalCost += minCostArray[noOfViews];
        }

        double endTime = System.currentTimeMillis();

        Iterator<Integer> iterator2 = viewsToSend.iterator();
        System.out.print("Tree set data: ");
        
        // Displaying the Tree set data
        while (iterator2.hasNext()) {
            System.out.print(iterator2.next() + " ");
        }
        System.out.println("\n");

        System.out.println("had received");
        iterator2 = viewsArr.iterator();

        while(iterator2.hasNext())
             System.out.print(iterator2.next() + " ");

        System.out.println("\n");


        System.out.println("The total no of views sent " +  viewsToSend.size());
        System.out.println("The total cost to send given set of views " + totalCost);
        System.out.println("The total time taken " + (endTime - startTime));

        Iterator getImageId = viewsToSend.iterator();

        try{

        while(getImageId.hasNext())
        {
            int imageId = (Integer) getImageId.next();
            /*try {
                int imageId = (Integer) getImageId.next();
                while(imageId != linesRead) {
                    encodedString = new String(readImage.readLine());
                    linesRead++;
                }*/
               // imagesToSend.put(imageId, new String(encodedString.getBytes(),"ISO-8859-1"));
                imagesToSend.put(imageId, null);
            /*}catch(Exception e) {
                System.out.println(e);
            }*/
        }

        }catch(Exception e){}
        try {

            ObjectOutputStream oos =  new ObjectOutputStream(currentSocket.getOutputStream()); 
            oos.writeObject(imagesToSend);
        } catch(Exception e) {
            System.out.println(e);
        }
    }

    public void run() {
        startComputations();
    }
    
    public static void main(String[] args) {
        


        ServerSocket serverSocket = null;
        Socket socket = null;

        try {
            //startPreProcessing();
            System.out.println("Preprocessing is done.");

            serverSocket = new ServerSocket(35360);
    
            while(true) {      
                MMDEA receiveViews = new MMDEA();           
                
                socket = serverSocket.accept();System.out.println("Received");
                receiveViews.currentSocket = socket;

                receiveViews.clientPort = socket.getPort();
                receiveViews.clientAddress =socket.getInetAddress();

                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                receiveViews.viewsArr = (TreeSet<Integer>) ois.readObject();
                receiveViews.requestSize = receiveViews.viewsArr.size();
                System.out.println("------"+receiveViews.viewsArr.size());
                receiveViews.start();

            }
        }catch(Exception e) {
                System.out.println(e);
        }

    }
}