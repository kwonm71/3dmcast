import java.io.*;
import java.util.*;
import java.net.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

public class  MMDEARouter extends Thread {

	static int qualityConstant = 4;
	static String host = "129.21.22.196";
	static int serverPort = 35360;
	static int noOfClients = 5; //The router will wait for these many requests before forwarding

	TreeSet<Integer> viewsToSend;
	ArrayList<ArrayList<Integer>> requestViews;
	ArrayList<InetAddress> clientAddress;
	ArrayList<Integer> clientPort;
	Socket[] currentSocket;

	public MMDEARouter() {
		viewsToSend = new TreeSet<Integer>();
		requestViews = new ArrayList<ArrayList<Integer>>();
		clientAddress = new ArrayList<InetAddress>();
		clientPort = new ArrayList<Integer>();
		currentSocket = new Socket[noOfClients];

		for(int i=0; i<noOfClients; i++) {
			currentSocket[i] = null;
		}
	}


    public void collectViews(ArrayList<Integer> requestViews) {
    	Iterator it = requestViews.iterator();

    	while (it.hasNext()) {
    		viewsToSend.add((Integer)it.next());    		
    	}
    }

    public static int findReplacingViewIndex(int searchView, int[] receivedViewsArr) {

    	for(int j=0;j<receivedViewsArr.length-1;j++) {
        	if(receivedViewsArr[j] < searchView && 
        		receivedViewsArr[j+1] > searchView) {
        			System.out.println(searchView + " is replaced by " + receivedViewsArr[j] + " and " + receivedViewsArr[j+1]);
        			return j;
        	}
        }

        return 0;
    }

    public void sendViews() {

    	try {

    		InetAddress serverIp = InetAddress.getByName(host);	

			Socket socket = new Socket(serverIp,serverPort);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(viewsToSend);

			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			HashMap<Integer, String> receivedViews = (HashMap<Integer, String>) ois.readObject();

			Set set = receivedViews.entrySet();
        	Iterator iterator = set.iterator();

			int[] receivedViewsArr = new int[receivedViews.size()];
			int i = 0;

			while(iterator.hasNext()) {
				Map.Entry me = (Map.Entry)iterator.next();
				receivedViewsArr[i] = (Integer) me.getKey();
				i++;
			}

			i=0;

			for(int j=0;j<receivedViewsArr.length; j++) {
				for(int k = j+1 ; k<receivedViewsArr.length; k++) {
					if(receivedViewsArr[k]<receivedViewsArr[j]) {
						int temp = receivedViewsArr[j];
						receivedViewsArr[j] = receivedViewsArr[k];
						receivedViewsArr[k] = temp;
					}
				}
			}
			for(int j=0; j<noOfClients; j++) {

				Iterator iterator2 = requestViews.get(j).iterator();
				HashMap<Integer, String> responseViews = new HashMap<Integer, String>();

				while(iterator2.hasNext()) {
					int x = (Integer) iterator2.next();
					if(!receivedViews.containsKey(x)) {
						int k = findReplacingViewIndex(x, receivedViewsArr);
						responseViews.put(receivedViewsArr[k], (String)receivedViews.get(receivedViewsArr[k]));
						responseViews.put(receivedViewsArr[k+1], (String)receivedViews.get(receivedViewsArr[k+1]));
					}
					else {
						responseViews.put(x, (String)receivedViews.get(x));
					}
				}

				oos = new ObjectOutputStream(currentSocket[j].getOutputStream());
				oos.writeObject(responseViews);
				sleep(200);
			}
		}catch(Exception e) {e.printStackTrace();
			System.out.println(e);
		}

    }

    public void run() {
    	sendViews();
    }

	public static void main(String[] args) {

		try {

			ArrayList<ArrayList<Integer>> requestViews = new ArrayList<ArrayList<Integer>>();
			ArrayList<InetAddress> clientAddress = new ArrayList<InetAddress>();
			ArrayList<Integer> clientPort = new ArrayList<Integer>();


			ServerSocket serverSocket = null;
        	Socket socket = null;

        	serverSocket = new ServerSocket(43555);

			while(true) {

				MMDEARouter sendViewBatch = new MMDEARouter();

				for(int i=0; i<noOfClients;i++) {

					socket = serverSocket.accept();
					sendViewBatch.currentSocket[i] = socket;
					ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
					requestViews.add(i, (ArrayList<Integer>) ois.readObject());
					clientAddress.add(i, socket.getInetAddress());
					clientPort.add(i, socket.getPort());
					sendViewBatch.collectViews(requestViews.get(i));
				}

				
				sendViewBatch.requestViews.addAll(requestViews);
				sendViewBatch.clientAddress.addAll(clientAddress);
				sendViewBatch.clientPort.addAll(clientPort);

				sendViewBatch.start();
			}

		}catch(Exception e) {
			System.out.println(e);
		}

	}
}